import React, { Component } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHeart } from '@fortawesome/free-solid-svg-icons'

export default class StandardCard extends Component {
    render() {
        const { info } = this.props;

        return (
            <div>
                <p className='title'>{info.title || ''}</p>
                <p className='message'>{info.message || ''}</p>
                <div className="status ">
                    <span>
                        <FontAwesomeIcon icon={faHeart} />  {info.like_count || 0} | 
                    </span>
                    <span> {info.comment_count || 0} Comments</span>
                    <p>Written By {info.author.first_name || ''} {info.author.last_name || ''}</p>

                </div>
            </div>
        )
    }
}
