import React, { Component } from 'react'
import EditableName from './EditableName.jsx'
import { connect } from 'react-redux'

 class UserName extends Component {
    constructor(props) {
        super(props)

        this.state = {
            isEdit: false
        }
    }

    showEdit = () => {
        this.setState({ isEdit: true })
    }

    closeEdit = () => {
        this.setState({ isEdit: false })
    }

    conditionUserName = () => {
        if (this.state.isEdit === true) {
            return <EditableName handleClose={this.closeEdit}></EditableName>
        } else {
            return (
                <>
                    <span>Welcome: {this.props.first_name} {this.props.last_name}  </span>
                    <span>
                        <button onClick={this.showEdit}>Edit</button>
                    </span>
                </>
            )
        }
    }
    render() {
        return (
            <div>
                { this.conditionUserName() }
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        first_name: state.first_name,
        last_name: state.last_name
    }
}

export default connect(mapStateToProps, null)(UserName);
