import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as CardsActions from '../state/action'


class EditableCard extends Component {
    constructor(props) {
        super(props)

        this.state = {
            title: '',
            message: ''
        }
    }

    componentDidMount() {  
        console.log('mount editable: ' + this.props.info.title) 

        this.setState({
            title: this.props.info.title,
            message: this.props.info.message
        })
    }


    changeHandler = e => {
        this.setState({
            title: this.input.value,
            message: this.textarea.value
        })
    }

    updateCard = (uid) => {

        console.log('update to ' + uid)
        console.log(`${this.state.title},\n ${this.state.message}`)
        this.props.updateCardRequest(uid, {...this.state})

        this.props.setEditable()
    }

    render() {
        const { info } = this.props;

        return (
            <div className='editPost'>
                <form >
                    <p className='title'>Edit Post</p>
                    <p>Author: { info.author.first_name} {info.author.last_name}</p>
                    <input 
                        type="text" 
                        ref={ip => this.input = ip} 
                        name='title' 
                        onChange={this.changeHandler}
                        defaultValue={info.title} 

                    /><br/>
                    <textarea 
                        name="message"
                        ref={ta => this.textarea = ta}
                        onChange={this.changeHandler}
                        defaultValue={info.message}
                    ></textarea><br/>
                </form>
                <button onClick={() => {this.updateCard(info.id)}}>Confirm</button>
                <button onClick={() => {this.props.setEditable()}}>Cancel</button>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(Object.assign({}, CardsActions), dispatch)
}

export default connect(null, mapDispatchToProps)(EditableCard);