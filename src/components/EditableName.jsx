import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as CardsActions from '../state/action'

class EditableName extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             first_name: '',
             last_name: ''
        }
    }
    
    componentDidMount() {
        this.setState({
            first_name: this.props.first_name,
            last_name: this.props.last_name
        })
    }

    setUserName = () => {
        const { first_name, last_name } = this.state;
        console.log({...this.state})
        this.props.setUserName({...this.state})
        this.props.handleClose()
    }

    changeHandler = (e) => {
        this.setState({
            first_name: this.first_name.value,
            last_name: this.last_name.value
        })
    }
    
    render() {
        return (
            <div>
                <span>Welcome: 
                    <input 
                        type="text"
                        placeholder='first name'
                        name='first_name'
                        ref={ip => this.first_name = ip}
                        defaultValue={this.state.first_name}
                        onChange={this.changeHandler}
                    />
                    <input 
                        type="text"
                        placeholder='last name'
                        ref={ip => this.last_name = ip}
                        defaultValue={this.state.last_name}
                        onChange={this.changeHandler}
                    />
                    <button onClick={this.setUserName}>Confirm</button>
                    <button onClick={this.props.handleClose}>Cancel</button>
                </span>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        first_name: state.first_name,
        last_name: state.last_name,
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(Object.assign({}, CardsActions), dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(EditableName);