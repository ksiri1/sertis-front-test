import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators, compose } from 'redux'
import * as CardsActions from '../state/action'

const initState = {
    title: '',
            message: '',
            comment_count: 0,
            like_count: 0
}
class AddCardModal extends Component {
    constructor(props) {
        super(props)

        this.state = initState
    }

    componentDidUpdate(prevProps) {
        if(prevProps.show !== this.props.show) {
            this.setState(initState)
        }
    }

    changeHandler = e => {
        this.setState({
            title: this.title.value,
            message: this.message.value,
            comment_count: this.comment_count.value,
            like_count: this.like_count.value
        })
    }

    addNewCard = () => {
        const { first_name, last_name } = this.props;
        const { title, message } = this.state;

        if (!title) {
            alert('Error: please fill title form')
        }  
        if (!message) {
            alert('Error: please fill message form')
        } 

        if(message && title ) {
            this.props.addNewCardRequest({
                ...this.state,
                author: {
                    first_name,
                    last_name
                }
            })
            this.props.handleClose()
        }
    }



    render() {
        return this.props.show && (
            <div className='modal'>
                <section className="modal-main">
                    <form action="">
                        <h3>Add New Card</h3>
                        <input
                            required
                            type="text"
                            ref={ip => this.title = ip}
                            name='title'
                            onChange={this.changeHandler}
                            placeholder='Title'
                        /><br />
                        <textarea
                            required
                            name="message"
                            ref={ta => this.message = ta}
                            onChange={this.changeHandler}
                            placeholder='Message'
                        ></textarea><br />
                        <input
                            type="number"
                            ref={ip => this.comment_count = ip}
                            name='comment_count'
                            placeholder='Comment Count'
                            onChange={this.changeHandler}
                        />
                        <input
                            type="number"
                            ref={ip => this.like_count = ip}
                            name='like_count'
                            placeholder='Like Count'
                            onChange={this.changeHandler}
                        />
                    </form>
                    <button onClick={this.addNewCard}>Confirm</button>
                    <button onClick={this.props.handleClose}>Cancel</button>
                </section>
            </div>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        first_name: state.first_name,
        last_name: state.last_name,
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(Object.assign({}, CardsActions), dispatch)
}


export default connect(mapStateToProps, mapDispatchToProps)(AddCardModal);