import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as CardsActions from '../state/action'

import AuthorCard from './AuthorCard.jsx'
import StandardCard from './StandardCard.jsx'

class Card extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isAuthor: false
        }
    }

    authenUser = () => {
        const { info, name } = this.props;

        const propsName = [...info.author.first_name, ...info.author.last_name].join("").toLowerCase();
        const userName = [...name.first_name, ...name.last_name].join("").toLowerCase();

        console.log(propsName === userName)
        if (propsName === userName) {
            console.log(`${propsName} === ${userName}`)
            this.setState({ isAuthor: true })
        } else {
            
            this.setState({ isAuthor: false})
        }
        
        
    }

    componentDidMount() {
        this.props.onRef(this)
        this.authenUser()
    }

    componentDidUpdate(prevProps) {
        const { first_name, last_name } = this.props;

        const prevName = [...prevProps.first_name, ...prevProps.last_name].join("").toLowerCase();
        const userName = [...first_name, ...last_name].join("").toLowerCase();

        if(prevName !== userName) {
            this.authenUser()
        }
    }

    conditionCard = () => {
        const { info } = this.props;

        console.log(info)
        if (this.state.isAuthor === true) {
            return <AuthorCard info={info}></AuthorCard>
        } else {
            return <StandardCard info={info}></StandardCard>
        }
    }

    render() {
        const { info } = this.props;

        return (
            <div className='card'>
                {this.conditionCard()}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        // username: state.username,
        first_name: state.first_name,
        last_name: state.last_name
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(Object.assign({}, CardsActions), dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Card);