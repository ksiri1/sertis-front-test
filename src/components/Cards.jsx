import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as CardsActions from '../state/action'

import Card from './Card.jsx'
import UserName from './UserName.jsx'
import Loader from './Loader.jsx'
class Cards extends Component {

    constructor(props) {
        super(props)

        this.state = {

        }
    }

    componentDidMount() {
        this.props.fetchCardsRequest();

    }

    // componentDidUpdate(prevProps) {
    //     const { first_name, last_name } = this.props;

    //     const prevName = [...prevProps.first_name, ...prevProps.last_name].join("").toLowerCase();
    //     const userName = [...first_name, ...last_name].join("").toLowerCase();

    //     console.log('prevProps: ')
    //     console.log(prevName)
    //     console.log(userName)
    //     if(prevName !== userName) {
    //         console.log('!!!!!!!!!!! state name changed')
    //         this.allCards(first_name, last_name)
    //     }
    // }

    

    allCards = (first_name, last_name) => {
        console.log('all Cardsssssssssssssssssssssssssss')
        const name = {
            first_name,
            last_name
        }
       
        const { cards, pending, error } = this.props
       
            return cards && cards.map((card, index) => {
                return <Card info={card} key={card.id} name={name} className='card'
                    onRef={ref => (this.singleCard = ref)}></Card>
            })
    }

    loadingCard = () => {
        const { cards, pending, error } = this.props
        if (pending === true) {
            return <Loader></Loader>
        } else {
            return (
                <>
                    <UserName></UserName>
                    <div className='cards'>
                        {this.allCards(this.props.first_name, this.props.last_name)}
                    </div>
                </>
            )
        }
    }


    render() {
        return (
            <div className='flex-column'>
                {this.loadingCard()}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        cards: state.cards,
        first_name: state.first_name,
        last_name: state.last_name,
        pending: state.pending
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(Object.assign({}, CardsActions), dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Cards);