
import { HashLoader } from 'react-spinners'

import React, { Component } from 'react'

export default class Loader extends Component {
    render() {
        return (
            <div>
                <HashLoader
                loading={true}></HashLoader>
            </div>
        )
    }
}

