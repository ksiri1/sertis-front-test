import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as CardsActions from '../state/action'

import StandardCard from './StandardCard.jsx'
import EditableCard from './EditableCard.jsx'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBan } from '@fortawesome/free-solid-svg-icons'

class AuthorCard extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             isEditable: false
        }
    }

    removeCard = (uid) => {
        console.log('click ' + uid)
         this.props.removeCardRequest(uid)
    }
    
    setEditable = () => {
         this.setState({ isEditable: !this.state.isEditable})
    }

    conditionCard = () => {
        const {info} =  this.props;

        if(this.state.isEditable === false) {
            return <>
                <div className="group-btn">
              
                <button onClick={() => {this.setEditable()}}>Edit</button>
                <button onClick={() => {this.removeCard(info.id)}}>
                    Remove
                </button>
                </div>
                <StandardCard info={info}></StandardCard>
                
            </>
        } else {
            return <EditableCard info={info} setEditable={this.setEditable}></EditableCard>
        }
    }

    render() {
        return (
            <>
                { this.conditionCard() }
            </>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(Object.assign({}, CardsActions), dispatch)
}

export default connect(null, mapDispatchToProps)(AuthorCard);
