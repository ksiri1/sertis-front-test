export const FETCH_CARDS_PENDING = 'FETCH_CARDS_PENDING';
export const FETCH_CARDS_SUCCESS = 'FETCH_CARDS_SUCCESS';
export const FETCH_CARDS_ERROR = 'FETCH_CARDS_ERROR';
export const EDITABLE_CARD = 'EDITABLE_CARD';
export const ADD_NEW_CARD = 'ADD_NEW_CARD';
export const SET_CARD = 'SET_CARD'
export const REMOVE_CARD = 'REMOVE_CARD'
export const SET_USERNAME = 'SET_USERNAME'
const API = `http://localhost:5555/cards/`

export const fetchCardsPending = () => {
    return {
        type: FETCH_CARDS_PENDING
    }
}

export const fetchCardsSuccess = (cards) => {
    return {
        type: FETCH_CARDS_SUCCESS,
        cards
    }
}

export const fetchCardsError = (error) => {
    return {
        type: FETCH_CARDS_ERROR,
        error
    }
}

export const addNewCard = (card) => {
    return {
        type: ADD_NEW_CARD,
        card
    }
}

export const setCard = (card) => {
    return {
        type: SET_CARD,
        card
    }
}

export const removeCard = (id) => {
    return {
        type: REMOVE_CARD,
        id
    }
}

export const setUserName = (author) => {
    return {
        type: SET_USERNAME,
        author
    }
}

export const updateCardRequest = (uid, json) => async (dispatch) => {
    try {
        console.log(`${json.title}, ${json.message}, ${uid}`)
        let res = await fetch(API + uid, {
                method: 'PATCH',
                body: JSON.stringify({
                    title: json.title,
                    message: json.message
                }),
                headers: {
                    'Content-Type': 'application/json',
                },
            })
            .then(res => {
                return res.json()
            })
            .then(response => {
                // console.log('response')
                // console.log(response.data)
                dispatch(setCard(response.data))
            })
            .catch(err => {
                console.log(err)
            })

    } catch (err) {
        console.log(err)
    }
}

export const addNewCardRequest = (obj) => async (dispatch) => {
    try {
        console.log(obj)
        await fetch(API, {
            method: 'POST',
            body: JSON.stringify({
                title: obj.title,
                message: obj.message,
                like_count: obj.like_count,
                comment_count: obj.comment_count,
                author: {
                    first_name: obj.author.first_name,
                    last_name: obj.author.last_name
                }
            }),
            headers: {
                'Content-Type': 'application/json',
            }
        }).then(res => {
            return res.json()
        }).then(response => {
            console.log(response.data)
            dispatch(addNewCard(response.data))
        })
    } catch (err) {
        console.log(err)
    }
}

export const removeCardRequest = (id) => async (dispatch) => {
    try {
        await fetch(API + id, {
            method: 'DELETE'
        }).then(res => {
            console.log('requet id: ' + id)
            dispatch(removeCard(id));
        })

    } catch (err) {
        console.log(err)
    }
}


export const fetchCardsRequest = () => async (dispatch) => {
    dispatch(fetchCardsPending())
    try {
        let response = await fetch(API)
        let json = await response.json();

        console.log(json.data)
        dispatch(fetchCardsSuccess(json.data))
    } catch (err) {
        console.log(err)
        dispatch(fetchCardsError(err))
    }

}