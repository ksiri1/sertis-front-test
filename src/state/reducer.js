import {
    FETCH_CARDS_PENDING,
    FETCH_CARDS_SUCCESS,
    FETCH_CARDS_ERROR,
    ADD_NEW_CARD,
    SET_CARD,
    REMOVE_CARD,
    SET_USERNAME

} from './action'

const initState = {
    // username: 'LindaDuncan',
    first_name: 'Austin',
    last_name: 'Simmons',
    cards: [],
    pending: false,
    error: null,
}

export const reducer = (state = initState, action) => {
    switch (action.type) {
        case FETCH_CARDS_PENDING:
            return Object.assign({}, state, {
                pending: true,
            })
        case FETCH_CARDS_SUCCESS:
            return Object.assign({}, state, {
                pending: false,
                cards: action.cards
            })
        case FETCH_CARDS_ERROR:
            return Object.assign({}, state, {
                error: action.error
            })
        case ADD_NEW_CARD:
            return Object.assign({}, state, {
                ...state,
                cards: [...state.cards, action.card]
            })
        case REMOVE_CARD: 
            return {
                ...state,
                cards: state.cards.filter((card) => 
                    card.id !== action.id
                )
            }
        case SET_CARD:
            return {
                ...state,
                cards: state.cards.map((card) =>
                    card.id === action.card.id ?
                    {
                        ...action.card
                    } :
                    card)
            }
        case SET_USERNAME:
            return Object.assign({}, state, {
                ...state,
                first_name: action.author.first_name,
                last_name: action.author.last_name
            })
        default:
                return state;
    }
}