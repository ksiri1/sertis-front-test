import React, { Component } from 'react'

import Cards from './components/Cards.jsx'
import AddCardModal from './components/AddCardModal.jsx'


export default class App extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             showAddNewCard: false
        }
    }

    showModalAddNewCard = () => {
        this.setState({ showAddNewCard: true })
    }

    closeModalAddNewCard = () => {
        this.setState({ showAddNewCard: false })
    }


    render() {
        return (
            <div>
                <AddCardModal 
                    show={this.state.showAddNewCard} 
                    handleClose={this.closeModalAddNewCard}
                ></AddCardModal>
                <div className="navbar">
                    <h2>SERTIS FRONTEND</h2>
                    <div className="navbar-menu">
                        <a href="#" onClick={this.showModalAddNewCard}>Add Card</a>
                    </div>
                </div>
                <div className="container">
                    <Cards></Cards>
                </div>
            </div>
        )
    }
}
